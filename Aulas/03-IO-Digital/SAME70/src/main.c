/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

//#define LED_PIO PIOC
//#define LED_PIO_ID 12
//#define LED_PIO_PIN 8
//#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 30
#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)

#define TOUCH_PIO PIOC
#define TOUCH_PIO_ID 12
#define TOUCH_PIO_PIN 13
#define TOUCH_PIO_PIN_MASK (1<< TOUCH_PIO_PIN)

#define FAN_PIO PIOC
#define FAN_PIO_ID 12
#define FAN_PIO_PIN 13
#define FAN_PIO_PIN_MASK (1<< FAN_PIO_PIN)

#define PRESENCE_PIO PIOC
#define PRESENCE_PIO_ID 12
#define PRESENCE_PIO_PIN 17
#define PRESENCE_PIO_PIN_MASK (1<< PRESENCE_PIO_PIN)



/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	delay_init();
	
	pmc_enable_periph_clk(TOUCH_PIO_ID);
	pmc_enable_periph_clk(PRESENCE_PIO_ID);
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_configure(TOUCH_PIO,PIO_INPUT, TOUCH_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(LED_PIO,PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(PRESENCE_PIO,PIO_INPUT, PRESENCE_PIO_PIN_MASK, PIO_DEFAULT);
	bool fan = true;
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while (1) {
		
		if(pio_get(PRESENCE_PIO, PIO_TYPE_PIO_INPUT, PRESENCE_PIO_PIN_MASK)){
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
			//delay_ms(1000);
			
		}
		else{
			//delay_ms(2000);
			pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			// pio set do ventilador == 0
			//delay_ms(1000);
		}
		
		if (pio_get(TOUCH_PIO, PIO_TYPE_PIO_INPUT, TOUCH_PIO_PIN_MASK)){
			fan = !fan;
			delay_ms(200);
			
			if(fan){
				//liga ventilador
				//pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			}
			else{
				// desliga ventilador
				//pio_set(LED_PIO, LED_PIO_PIN_MASK);
			}
			
			
		}
			
		else{
			
		}
	}
	return 0;
}
