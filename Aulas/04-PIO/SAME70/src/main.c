/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOA
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)
#define LED_PIO_PIOC 


#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

Bool flag = false;

void button_callback(){
	flag = true;
}





// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_configure(PIOC,PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO,PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_enable_interrupt(LED_PIO, LED_PIO_PIN_MASK);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
	
	
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, button_callback());
	
	while (1) {
		if(flag){
				pio_set(PIOC, LED_PIO_PIN_MASK);
				delay_ms(500);
				pio_clear(PIOC, LED_PIO_PIN_MASK);
		}
		flag = false;
	}
	return 0;
}
