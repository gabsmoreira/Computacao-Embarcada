/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

//#define LED_PIO PIOC
//#define LED_PIO_ID 12
//#define LED_PIO_PIN 8
//#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 30
#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)

#define TOUCH_PIO PIOC
#define TOUCH_PIO_ID 12
#define TOUCH_PIO_PIN 13
#define TOUCH_PIO_PIN_MASK (1<< TOUCH_PIO_PIN)

#define FAN_PIO PIOC
#define FAN_PIO_ID 12
#define FAN_PIO_PIN 31
#define FAN_PIO_PIN_MASK (1<< FAN_PIO_PIN)

#define PRESENCE_PIO PIOC
#define PRESENCE_PIO_ID 12
#define PRESENCE_PIO_PIN 17
	#define PRESENCE_PIO_PIN_MASK (1<< PRESENCE_PIO_PIN)



void led_on(){
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
}

void led_off(){
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
}

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	delay_init();
	
	// Iniciando os perifericos
	pmc_enable_periph_clk(TOUCH_PIO_ID);
	pmc_enable_periph_clk(PRESENCE_PIO_ID);
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(FAN_PIO_ID);
	
	// Configurando cada periferico
	pio_configure(TOUCH_PIO,PIO_INPUT, TOUCH_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(LED_PIO,PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(PRESENCE_PIO,PIO_INPUT, PRESENCE_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(FAN_PIO,PIO_OUTPUT_0, FAN_PIO_PIN_MASK, PIO_DEFAULT);
	
	// Flag do ventilador
	bool fan = true;
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while (1) {
		
		if(pio_get(PRESENCE_PIO, PIO_TYPE_PIO_INPUT, PRESENCE_PIO_PIN_MASK)){
			led_on();	
		}
		else{
			led_off();
			//pio_clear(FAN_PIO, FAN_PIO_PIN_MASK);
			// desliga ventilador
			//delay_ms(1000);
			//pio_clear(FAN_PIO, FAN_PIO_PIN_MASK);
			
		}
		
		if (pio_get(TOUCH_PIO, PIO_TYPE_PIO_INPUT, TOUCH_PIO_PIN_MASK)){
			fan = !fan;
			delay_ms(200);
			
			if(fan){
				pio_set(FAN_PIO, FAN_PIO_PIN_MASK);
				//liga ventilador
				//pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			}
			else{
				pio_clear(FAN_PIO, FAN_PIO_PIN_MASK);
				// desliga ventilador
				//pio_set(LED_PIO, LED_PIO_PIN_MASK);
			}
			
			
		}
			
		else{
			
		}
	}
	return 0;
}
