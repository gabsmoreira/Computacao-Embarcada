/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOA
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1<< LED_PIO_PIN)
#define LED_PIO_PIOC 


#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

Bool flag = false;

void button_callback(){
    flag = !flag;
}


void _pio_set(Pio *p_pio, const uint32_t ul_mask){
	p_pio->PIO_SODR = ul_mask;
}

void _pio_clear(Pio *pio, const uint32_t ul_mask){
	p_pio->PIO_CODR = ul_mask;
} 

void _pio_pull_up(Pio *pio, const uint32_t ul_mask, const uint32_t ul_pull_up_enable){
	if(ul_pull_up_enable == 1){
		p_pio->PIO_PUER = ul_mask;
	}
	else{
		p_pio->PIO_PUDR = ul_mask;
	}

}

void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level, const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable){
	p_pio->PIO_PER = ul_mask;
	p_pio->PIO_OER = ul_mask;

	if(ul_default_level){
		_pio_set(p_pio, ul_mask);
	}
	else{
		_pio_clear(p_pio, ul_mask);
	}

	if(ul_multidrive_enable){
		p_pio ->PIO_MDER = ul_mask;
	}
	else{
		p_pio ->PIO_MDDR = ul_mask;
	}

	_pio_pull_up(p_pio, ul_mask, ul_pull_up_enable);
}

void _pio_set_input(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_attribute){
	p_pio -> PIO_ODR = ul_mask;
	if(ul_attribute & _PIO_DEFAULT){
		continue;
	}
	if(ul_attribute & _PIO_PULLUP){
		_pio_pull_up(p_pio, ul_mask, 1);
	}
	if((ul_attribute & _PIO_DEGLITCH) != 0){
		p_pio -> PIO_IFSCDR = ul_mask;
	}
	if((ul_attribute & _PIO_OPENDRAIN) != 0){
		p_pio ->PIO_MDER = ul_mask;
	}
	if((ul_attribute & _PIO_DEBOUNCE) != 0){
		p_pio -> PIO_IFSCER = ul_mask;
	}

}




// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_configure(PIOA, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_DEFAULT);
    _pio_pull_up(BUT_PIO, BUT_PIO_PIN_MASK, 1);

		
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_RISE_EDGE, button_callback());
	
	while (1) {
		if(flag){
			_pio_set(PIOC, LED_PIO_PIN_MASK);
			delay_ms(500);
			_pio_clear(PIOC, LED_PIO_PIN_MASK);
		}
		flag = false;
	}
	return 0;
}
