#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);




#define LED1_PIO_ID	   ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		   0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID	   ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		   30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID	   ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		   2
#define LED3_PIN_MASK   (1<<LED3_PIN)

/**
* Boto
*/

#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO				  PIOD
#define BUT1_PIN				  28
#define BUT1_PIN_MASK			  (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE  79

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO				  PIOC
#define BUT2_PIN				  31
#define BUT2_PIN_MASK			  (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE  79

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO				  PIOA
#define BUT3_PIN				  19
#define BUT3_PIN_MASK			  (1 << BUT3_PIN)
#define BUT3_DEBOUNCING_VALUE  79

void but_callback(void);
void init_but_board(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;
SemaphoreHandle_t xSemaphore3;


void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}


/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}



/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */
static void task_led1(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	volatile bool on = true;
	const TickType_t xDelay = 125 / portTICK_PERIOD_MS;
	
	xSemaphore1 = xSemaphoreCreateBinary();

	if (xSemaphore1 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(on){
			pin_toggle(LED1_PIN, LED1_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphore1, ( TickType_t ) 0) == pdTRUE ){
			pin_toggle(LED1_PIN, LED1_PIN_MASK);
			
			if (on){
				on = false;
				pio_set(LED1_PIN, LED1_PIN_MASK);
				
				
			}
			else{
				on = true;
			}
		}
	}
}

static void task_led2(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	volatile bool on = true;
	const TickType_t xDelay = 250 / portTICK_PERIOD_MS;
	
	xSemaphore2 = xSemaphoreCreateBinary();

	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(on){
			pin_toggle(LED2_PIN, LED2_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphore2, ( TickType_t ) 0) == pdTRUE ){
			pin_toggle(LED2_PIN, LED2_PIN_MASK);
			
			if (on){
				on = false;
				pio_set(LED2_PIN, LED2_PIN_MASK);
				
			
			}
			else{
				on = true;
			}
		}
	}
}

static void task_led3(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	volatile bool on = true;
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	
	xSemaphore3 = xSemaphoreCreateBinary();

	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(on){
			pin_toggle(LED3_PIN, LED3_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphore3, ( TickType_t ) 0) == pdTRUE ){
			pin_toggle(LED3_PIN, LED3_PIN_MASK);
			
			if (on){
				on = false;
				pio_set(LED3_PIN, LED3_PIN_MASK);
				
			}
			else{
				on = true;
			}
		}
	}
}

void but1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but1_callback \n");
	xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
	printf("semaforo 1 tx \n");
}

void but2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but2_callback \n");
	xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
	printf("semaforo 2 tx \n");
}

void but3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but3_callback \n");
	xSemaphoreGiveFromISR(xSemaphore3, &xHigherPriorityTaskWoken);
	printf("semaforo 3 tx \n");
}



void but_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrupco do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};

void LED_init(){
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, 1, 0, 0);
	pio_set_output(LED3_PIO, LED3_PIN_MASK, 1, 0, 0);
};

void but_init_all(){
    but_init(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN, BUT1_PIN_MASK, but1_callback);
	but_init(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN, BUT2_PIN_MASK, but2_callback);
	but_init(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN, BUT3_PIN_MASK, but3_callback);
}



/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	/* iniciliza botao */
    but_init_all();
	LED_init();


	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task 1\r\n");
	}
	
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task 2\r\n");
	}
	
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task 3\r\n");
	}


	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;

}